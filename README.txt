CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation
 * Configuration

INTRODUCTION
------------

Notify bar module provide you functionality to show the notification bar at the page top
and also provide the block with name "Notify bar" to show on the site.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/notify_bar

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/notify_bar

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit:
   https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules
   for further information.

CONFIGURATION
-------------

 * Configure notify bar message in Administration » Structure » Configuration » Notify bar:

   - Update the notify bar content

     Add your message, button text, button link, background colour and text colour.

   - Hide the notify bar from page top

     You can hide the notification bar from top to select the none option from the "Show notify bar" bar drop-down.

   - Add block on region

     You can use the block("Notify bar") to show in the specific region.
