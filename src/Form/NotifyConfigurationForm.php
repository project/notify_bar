<?php

namespace Drupal\notify_bar\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configuration form definition for the notification bar.
 */
class NotifyConfigurationForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['notify_bar.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'notify_configuration_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $config = $this->config('notify_bar.settings');
    $form['message'] = [
      '#type' => 'details',
      '#title' => $this->t('Message'),
      '#open' => TRUE,
    ];
    $form['message']['description'] = [
      '#type' => 'text_format',
      '#title' => $this->t('Message'),
      '#description' => $this->t('Enter Notify Bar Message'),
      '#format' => $config->get('description.format'),
      '#default_value' => $config->get('description.value'),
    ];
    $form['message']['cta'] = [
      '#type' => 'details',
      '#title' => $this->t('CTA'),
      '#open' => TRUE,
    ];
    $form['message']['cta']['notify_cta_text'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#default_value' => $config->get('notify_cta_text'),
    ];
    $form['message']['cta']['notify_cta_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('URL'),
      '#description' => $this->t('<span>You can enter an internal path such as /node/add or an external URL such as http://example.com.</span>'),
      '#default_value' => $config->get('notify_cta_url'),
    ];
    $form['message']['style'] = [
      '#type' => 'details',
      '#title' => $this->t('Style'),
      '#open' => TRUE,
    ];
    $form['message']['style']['background'] = [
      '#type' => 'color',
      '#title' => $this->t('Background'),
      '#default_value' => $config->get('background'),
    ];
    $form['message']['style']['color'] = [
      '#type' => 'color',
      '#title' => $this->t('Text Color'),
      '#default_value' => $config->get('color'),
    ];
    $form['message']['display'] = [
      '#type' => 'details',
      '#title' => $this->t('Display'),
      '#open' => TRUE,
    ];
    $form['message']['display']['notify_show'] = [
      '#type' => 'select',
      '#title' => $this->t('Show notify bar'),
      '#options' => [
        'top' => $this->t('Page Top'),
      ],
      '#empty_option' => $this->t('- None -'),
      '#default_value' => $config->get('notify_show'),
    ];
    $form['message']['display']['notify_visibility'] = [
      '#type' => 'select',
      '#title' => $this->t('Notify bar Visibility'),
      '#options' => [
        'all_pages' => $this->t('All pages'),
        'front' => $this->t('Front page'),
      ],
      '#default_value' => $config->get('notify_visibility'),

    ];
    $form['message']['display']['notify_visibility']['#states'] = [
      'visible' => [
        [
          [':input[name="notify_show"]' => ['value' => 'top']],
        ],
      ],
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $this->config('notify_bar.settings')
      ->set('description.value', $values['description']['value'])
      ->set('description.format', $values['description']['format'])
      ->set('notify_cta_text', $values['notify_cta_text'])
      ->set('notify_cta_url', $values['notify_cta_url'])
      ->set('background', $values['background'])
      ->set('color', $values['color'])
      ->set('notify_show', $values['notify_show'])
      ->set('notify_visibility', $values['notify_visibility'])
      ->save();
    parent::submitForm($form, $form_state);

  }

}
