<?php

namespace Drupal\notify_bar\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\notify_bar\NotifyBarInfo;

/**
 * Notify bar block.
 *
 * @Block(
 *   id = "notify_bar_info",
 *   admin_label = @Translation("Notify Bar")
 * )
 */
class NotifyBarBlock extends BlockBase implements ContainerFactoryPluginInterface {
  /**
   * Notify bar info.
   *
   * @var \Drupal\notify_bar\NotifyBarInfo
   */
  protected $notifyInfo;

  /**
   * Construct.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param string $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\notify_bar\NotifyBarInfo $notifyInfo
   *   The notifyInfo service.
   */
  public function __construct(array $configuration,
    $plugin_id,
    $plugin_definition,
    NotifyBarInfo $notifyInfo) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->notifyInfo = $notifyInfo;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
    $configuration,
    $plugin_id,
    $plugin_definition,
    $container->get('notify_bar.info')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $this->info = $this->notifyInfo->getNotify();
    if (!empty($this->info)) {
      $message = $this->info->get("description.value");
      $btn_text = $this->info->get("notify_cta_text");
      $btn_url = $this->info->get("notify_cta_url");
      $background = $this->info->get("background");
      $color = $this->info->get("color");
      return [
        '#theme' => 'notify_bar_info',
        '#attached' => ['library' => ['notify_bar/notify-bar-layout']],
        '#message' => $message,
        '#btnText' => $btn_text,
        '#btnUrl' => $btn_url,
        '#background' => $background,
        '#color' => $color,
      ];
    }
    else {
      return [
        '#markup' => $this->t('Configuration data missing....'),
      ];
    }
  }

}
