<?php

namespace Drupal\notify_bar;

use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * Notify bar services.
 */
class NotifyBarInfo {
  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * NotifyBarInfo constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->configFactory = $config_factory;
  }

  /**
   * Returns the config.
   */
  public function getNotify() {
    $config = $this->configFactory->get('notify_bar.settings');
    if ($config != "") {
      return $config;
    }
  }

}
